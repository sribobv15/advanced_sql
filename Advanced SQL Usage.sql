-- Create Tables
CREATE TABLE warehouses (
    warehouse_id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    location VARCHAR(100) NOT NULL
);

CREATE TABLE carriers (
    carrier_id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    contact_person VARCHAR(50),
    contact_number VARCHAR(15)
);

CREATE TABLE shipments (
    shipment_id SERIAL PRIMARY KEY,
    tracking_number VARCHAR(20) UNIQUE NOT NULL,
    weight DECIMAL(10, 2) NOT NULL,
    status VARCHAR(20) DEFAULT 'Pending',
    warehouse_id INT REFERENCES warehouses(warehouse_id),
    carrier_id INT REFERENCES carriers(carrier_id)
);
	

-- insert the data in to the tables
INSERT INTO warehouses (name, location) VALUES
    ('Warehouse A', 'Location A'),
    ('Warehouse B', 'Location B');

SELECT * FROM warehouses;

INSERT INTO carriers (name, contact_person, contact_number) VALUES
    ('Carrier X', 'John Doe', '123-456-7890'),
    ('Carrier Y', 'Jane Smith', '987-654-3210');

SELECT * FROM carriers;

INSERT INTO shipments (tracking_number, weight, warehouse_id, carrier_id) VALUES
    ('ABC123', 150.5, 1, 1),
    ('XYZ789', 200.0, 2, 2);

SELECT * FROM shipments;
-- Views

-- View to display tracking number, weight, status of shipments, and warehouse name
CREATE VIEW warehouse_shipments AS
SELECT 
    tracking_number,
    weight,
    status,
    (SELECT name FROM warehouses WHERE warehouse_id = shipments.warehouse_id) AS warehouse_name
FROM shipments;


-- View to display tracking number, weight, status of shipments, and carrier name

CREATE VIEW carrier_shipments AS
SELECT s.tracking_number, s.weight, s.status, c.name AS carrier_name
FROM shipments s
INNER JOIN carriers c ON s.carrier_id = c.carrier_id;



-- Common Table Expressions (CTEs)

-- CTE to retrieve pending shipments with tracking number, weight, and warehouse location
WITH pending_shipments AS (
    SELECT s.tracking_number, s.weight, w.location AS warehouse_location
    FROM shipments s
    INNER JOIN warehouses w ON s.warehouse_id = w.warehouse_id
    WHERE s.status = 'Pending'
)


-- Create a CTE named heavy_shipments that includes the tracking number, weight, and carrier name for all shipments with a weight greater than 200.

WITH heavy_shipments AS (
    SELECT s.tracking_number, s.weight, c.name AS carrier_name
    FROM shipments s
    INNER JOIN carriers c ON s.carrier_id = c.carrier_id
    WHERE s.weight > 200
)

-- Transactions

-- Transaction to update the status of shipment 'ABC123' to 'Shipped' and increment weight by 10 units
BEGIN;
    UPDATE shipments
    SET status = 'Shipped', weight = weight + 10
    WHERE tracking_number = 'ABC123';
COMMIT;

SELECT * FROM shipments;
--Write another transaction that inserts a new shipment with tracking number 'LMN456', weight 180.75, into Warehouse B using Carrier Y.

BEGIN;
    INSERT INTO shipments (tracking_number, weight, warehouse_id, carrier_id)
    VALUES ('LMN456', 180.75, (SELECT warehouse_id FROM warehouses WHERE name = 'Warehouse B'), (SELECT carrier_id FROM carriers WHERE name = 'Carrier Y'));
COMMIT;

-- Index

-- Create an index on tracking_number column for faster retrieval
CREATE INDEX idx_tracking_number ON shipments (tracking_number);




